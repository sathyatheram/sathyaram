---
title: 'About'
section_slug: 'about'
photo_file: 'me.jpg'
photo_alt: 'Me'
about_title: I'm Sathya Ram,<br/>a Web Designer & Frontend<br/>Developer in D.C.
about_description: 
  - 'I am an enthusiastic triple threat: Designer, Developer, Photographer with a critical, caring and open-minded eye for pixel perfection. My charisma and drive allow me to craft exciting graphics and crisp code while pushing the confides of creativity.'
services:
  -
    href: '#web'
    class: 'web'
    letter: 'W'
    title: 'Websites'
    back: 'I’m an end-to-end developer who specializes in Drupal websites. I’ve developed responsive/adaptive custom Wordpress themes with HTML5, CSS3, SASS, PHP and jQuery. I’m currently in the intensive General Assembly coding boot camp learning Javascript, React.js and Python. I create crisp code with a critical eye for detail. My skills are only surpassed by my passion.'
  -
    href: '#graphic'
    class: 'graphic'
    letter: 'G'
    title: 'Graphics'
    back: 'I’m a graphic designer who pushes the confines of creativity as I strive for pixel perfection. I care strongly about user experiences and make sure users are at the forefront of my mind every step of the way. If you’re looking for a cookie-cutter designer -look elsewhere. I aim to make designs that are fresh and advance the industry.'
  -
    href: '#photography'
    class: 'photography'
    letter: 'P'
    title: 'Photos'
    back: 'I’m experienced in a wide range of photography. My portfolio ranges for portraits to food to landscapes to space. My photos can be found on a variety of websites I’ve helped revitalize, such as Lehigh University’s website and Fudtruck. Having photography skills has taken my website and graphic designs to a new level.'
---

