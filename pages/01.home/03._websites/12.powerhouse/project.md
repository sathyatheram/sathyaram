---
title: 'Powerhouse'
tagline: "Lehigh's Tech Hub"
section_classes: ''
bg_style: 'linear-gradient(to right, rgba(80,80,80, 0.66), rgba(110,110,110, 0.66)), url(/user/themes/sathyaram/images/web/powerhouse.jpg)'
pre_html: ''
taxonomy:
  tag:
    - Drupal 7
    - HTML/CSS/jQuery
    - Web Design
    - Graphic Design
link: https://powerhouse.lehigh.edu
dribbble: https://dribbble.com/shots/5159433-Powerhouse-Website
---
Lehigh University's premiere place for fully equipped iMac Computers, PCs with CAD/Solidworks, 3D Printers and student work spaces. They needed a website to showcase all their services and facilities and I made it happen.
