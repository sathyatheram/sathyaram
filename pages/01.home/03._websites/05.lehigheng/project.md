---
title: 'Lehigh Engineering'
tagline: 'P.C. Rossin College of Engineering'
section_classes: ''
bg_style: 'linear-gradient(rgba(27, 117, 186, 0.66), rgba(19, 188, 255, 0.66)), url(/user/themes/sathyaram/images/web/engbg.jpg)'
pre_html: ''
taxonomy:
  tag:
    - Web Design
    - HTML/CSS Theming
    - Branding
    - Graphic Design
---
While at Lehigh, I had the honor of redesigning Lehigh's Engineering College website. Starting from the ground up, I aimed for a vibrant, contemporary aesthetic that would catch the eye of a new budding engineer as well as push the limits for how collegiate websites should look and function.