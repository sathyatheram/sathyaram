---
title: 'Technical Entrepreunership'
tagline: "Program Website"
section_classes: ''
bg_style: 'linear-gradient(to right, rgba(64, 195, 241, 0.66) 0%, rgba(126, 194, 66, 0.66) 50%, rgba(252, 183, 21, 0.66) 100%), url(/user/themes/sathyaram/images/web/te.jpg)'
pre_html: ''
taxonomy:
  tag:
    - Drupal 7
    - HTML/CSS/jQuery
    - Web Design
    - Graphic Design
link: https://te.lehigh.edu
dribbble: https://dribbble.com/shots/5124149-Technical-Entrepreneurship-Website

---
As a student, my interest in web design and development led me to get the opportunity to create the Technical Entrepreurship program's website. It's one of my first sites!
