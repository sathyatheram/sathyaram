---
title: 'Lehigh 2018 Theme'
tagline: "Lehigh University's Official Website Theme"
section_classes: 'full-width'
bg_style: 'linear-gradient(to right, rgba(255,255,255, 0.45), rgba(255,255,255, 0.45)), url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/481345/teacher_student-compressor.jpg)'
pre_html: ''
taxonomy:
  tag:
    - Web Design
    - HTML/CSS Theming
    - Drupal 7
    - Branding
    - Graphic Design
---
I created Lehigh University's Official 2018 Theme that is set to be used across over 150 websites. It was a personal and exhilerating project that required a lot of research, reiterating, and understanding. But I was able get a flexible, scalable, lightweight, theme out that would bring Lehigh out of their old theme created in 2012. 
